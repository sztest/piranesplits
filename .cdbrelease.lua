-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

return {
	user = "Warr1024",
	pkg = "piranesplits",
	dev_state = "ACTIVELY_DEVELOPED",
	version = stamp .. "-$Format:%h$",
	type = "mod",
	title = "Piranesi Speedrun Splits",
	short_description = "Detailed split timer for Piranesi speedrunning",
	tags = {"singleplayer"},
	content_warnings = {},
	license = "MIT",
	media_license = "MIT",
	long_description = readtext("README.md"),
	repo = "https://gitlab.com/sztest/piranesplits",
	issue_tracker = "https://gitlab.com/sztest/piranesplits/-/issues",
	maintainers = {"Warr1024"},
	screenshots = {readbinary(".cdb-screen.webp")}
}

-- luacheck: pop

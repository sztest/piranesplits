A detailed split timer for Piranesi Restoration Project speedrunning.

> **SPOILER WARNING**: This mod is intended for use by speedrunners, and output it generates may contain spoilers for the game.  It is not recommended for casual play.

A split is created whenever:

- The player navigates to a new room (at the half-way point of the connecting hallway).
- The player makes a "discovery" (anything that increases score: key item, puzzle, enters a new room).

Splits are saved to:

- Internal mod storage (authoritative).
- A JSON file named `piranesplits_###.json` in the world dir, for external use.  The `###` in the filename is a timestamp of the time at which the run was started.
- Displayed on screen in the chat, in the format of `total time | diff time | score | label`.

The splits that are timed are generated dynamically based on the route taken, so runs with different routes are not easily comparable.  The more detailed split timing is helpful for cost analysis for route planning.

A run is started at the time the world is first loaded.  Quitting and reloading the world will continue the existing run.

To restart a run, the simplest method is to delete and recreate the world.  It's also possible to clear the map, mod_storage, and player databases, but this is more error-prone and not recommended.

The timer uses in-game time only, so can only work for RTA runs if done without quitting/reloading.

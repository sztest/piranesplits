-- LUALOCALS < ---------------------------------------------------------
local math, minetest, os, pairs, piredo_map, piredo_player,
      piredo_schems, string
    = math, minetest, os, pairs, piredo_map, piredo_player,
      piredo_schems, string
local math_floor, os_time, string_format
    = math.floor, os.time, string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local modstore = minetest.get_mod_storage()

local db
do
	local s = modstore:get_string("db")
	db = s and minetest.deserialize(s) or {}
end
db.splits = db.splits or {}
db.splitcount = db.splitcount or {}
db.runid = db.runid or os_time()

local savedb
do
	local jsonpath = string_format("%s/%s_%d.json",
		minetest.get_worldpath(), modname, db.runid)
	savedb = function()
		modstore:set_string("db", minetest.serialize(db))
		minetest.safe_file_write(jsonpath, minetest.write_json(db.splits))
	end
end

local showsplit
do
	local function timefmt(s)
		local h = math_floor(s / 3600)
		s = s - h * 3600
		local m = math_floor(s / 60)
		s = s - m * 60
		return string_format("%02d:%02d:%06.3f", h, m, s)
	end

	showsplit = function(i, pname)
		local split = db.splits[i]
		local oldwhen = i > 1 and db.splits[i - 1].when or 0
		local disp = split.label
		if split.count > 1 then
			disp = string_format("%s #%d", disp, split.count)
		end
		local line = string_format("%s | %s | %02d | %s",
			timefmt(split.when),
			string_format("%07.3f", split.when - oldwhen),
			split.score,
			disp
		)
		return minetest.chat_send_player(pname, line)
	end
end

local enableshow = {}
minetest.register_on_joinplayer(function(player)
		local name = player:get_player_name()
		for i = 1, #db.splits do
			showsplit(i, name)
		end
		enableshow[player:get_player_name()] = true
	end)
minetest.register_on_leaveplayer(function(player)
		enableshow[player:get_player_name()] = nil
	end)

local lookup = {
	start = "Start",
	reload = "Reload",
	["solve:candles"] = "Solve Candle Sequence",
	["solve:clock_room"] = "Solve Clock Room",
	["solve:crowns"] = "Solve Crowns",
	["solve:door_black"] = "Unlock Prison Door",
	["solve:door_gold"] = "Unlock Theater Door",
	["solve:final"] = "Solve Final Altar",
	["solve:final_warp"] = "Escape Mansion",
	["solve:flowers"] = "Solve Graveyard Flowers",
	["solve:melt:piredo_terrain:piranesi__swordblock"] = "Melt Pewter Sword",
	["solve:melt:piredo_terrain:piranesi__plateblock"] = "Melt Pewter Plate",
	["solve:melt:piredo_terrain:piranesi__keyblock_metal"] = "Melt Pewter Key",
	["solve:gear_mold"] = "Mold Pewter Gear",
	["solve:gear_quench"] = "Quench Pewter Gear",
	["solve:gear_extract"] = "Extract Pewter Gear",
	["solve:gear_install"] = "Install Pewter Gear",
	["solve:machine_start"] = "Solve Forest Machine",
	["solve:brewed_rybgyrb"] = "Brew Black Potion",
	["solve:bottled_rybgyrb"] = "Bottle Black Potion",
	["solve:bowl_black"] = "Pour Black Potion",
	["solve:brewed_gbry"] = "Brew Purple Potion",
	["solve:bottled_gbry"] = "Bottle Purple Potion",
	["solve:bowl_purple"] = "Pour Purple Potion",
}
minetest.after(0, function()
		for k, v in pairs(minetest.registered_items) do
			lookup["inv:" .. k] = "Get " .. (v.description or k)
			lookup["read:" .. k] = "Read " .. (v.piredo_clues_fulldesc
				or v.description or k)
		end
		for k, v in pairs(piredo_schems) do
			local title = v.title or k
			lookup["nav:" .. k] = "Load " .. title
			lookup["room:" .. k] = "Enter " .. title
		end
	end)

local function addsplit(key)
	local newcount = (db.splitcount[key] or 0) + 1
	db.splitcount[key] = newcount
	db.splits[#db.splits + 1] = {
		key = key,
		label = lookup[key] or key,
		count = newcount,
		score = piredo_player.score,
		when = piredo_player.timer,
	}
	savedb()
	for name in pairs(enableshow) do
		showsplit(#db.splits, name)
	end
end

do
	local function intialsplit()
		if not piredo_player.timer then
			return minetest.after(0, intialsplit)
		end
		addsplit(#db.splits == 0 and "start" or "reload")
	end
	intialsplit()
end

do
	local olddiscover = piredo_player.discover
	function piredo_player.discover(label, ...)
		local oldscore = piredo_player.score
		local function helper(...)
			if piredo_player.score > oldscore then
				addsplit(label)
			end
			return ...
		end
		return helper(olddiscover(label, ...))
	end
end

do
	local oldsetroom = piredo_map.set_current_room
	function piredo_map.set_current_room(pos, room, ...)
		if room == db.lastnav then
			return oldsetroom(pos, room, ...)
		end
		db.lastnav = room
		addsplit("nav:" .. room)
		return oldsetroom(pos, room, ...)
	end
end
